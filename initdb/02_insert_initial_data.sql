INSERT INTO route_info (departure_lon, departure_lat, departure_name, destination_lon, destination_lat, destination_name) VALUES
('30.298056', '60.034631', 'Kolomagy', '30.274189', '59.799664', 'Airport Pulkovo'),
('30.298056', '60.034631', 'Kolomagy', '30.462800', '59.874354', 'Gorachiy home'),
('30.298056', '60.034631', 'Kolomagy', '30.304653', '59.965125', 'Czirulnik'),
('30.298056', '60.034631', 'Kolomagy', '30.288060', '60.002352', 'Pionerka');
