CREATE TABLE IF NOT EXISTS route_info (
    id SERIAL PRIMARY KEY,
    departure_lon VARCHAR(31) NOT NULL,
    departure_lat VARCHAR(31) NOT NULL,
    departure_name VARCHAR(31) NOT NULL,
    destination_lon VARCHAR(31) NOT NULL,
    destination_lat VARCHAR(31) NOT NULL,
    destination_name VARCHAR(31) NOT NULL
);



CREATE TABLE IF NOT EXISTS route_data (
    id SERIAL PRIMARY KEY,
    created TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    class_name VARCHAR(31) NOT NULL,
    price INTEGER NOT NULL,
    distance INTEGER NOT NULL,
    trip_time INTEGER NOT NULL,
    waiting_time INTEGER,
    route_id INTEGER NOT NULL,
    FOREIGN KEY (route_id) REFERENCES route_info(id)
);
