package com.lebedev.taxitripdata.repository;

import com.lebedev.taxitripdata.model.entity.Route;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RouteRepository extends JpaRepository<Route, Integer> {
}
