package com.lebedev.taxitripdata.repository;

import com.lebedev.taxitripdata.model.entity.RouteData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RouteDataRepository extends JpaRepository<RouteData, Integer> {
}
