package com.lebedev.taxitripdata.mapper;

import com.lebedev.taxitripdata.model.entity.Route;
import com.lebedev.taxitripdata.model.reponse.RouteResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RouteConverter implements Converter<Route, RouteResponse> {
    @Override
    public RouteResponse convert(Route source) {
        return new RouteResponse()
                .setId(source.getId())
                .setDepartureLongitude(source.getDepartureLongitude())
                .setDepartureLatitude(source.getDepartureLatitude())
                .setDepartureName(source.getDepartureName())
                .setDestinationLongitude(source.getDestinationLongitude())
                .setDestinationLatitude(source.getDestinationLatitude())
                .setDestinationName(source.getDestinationName());
    }
}
