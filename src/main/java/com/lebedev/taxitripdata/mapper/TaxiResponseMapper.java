package com.lebedev.taxitripdata.mapper;

import com.lebedev.taxitripdata.model.entity.Route;
import com.lebedev.taxitripdata.model.entity.RouteData;
import com.lebedev.taxitripdata.model.reponse.Option;
import com.lebedev.taxitripdata.model.reponse.TaxiResponse;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class TaxiResponseMapper {
    public List<RouteData> toRouteDataList(TaxiResponse response, Route route) {
        List<RouteData> result = new ArrayList<>(response.getOptions().size());
        for (Option option : response.getOptions()) {
            result.add(new RouteData()
                    .setCreated(LocalDateTime.now())
                    .setClassName(option.getClass_name())
                    .setPrice((int) Math.round(option.getPrice()))
                    .setDistance((int) Math.round(response.getDistance()))
                    .setTripTime((int) Math.round(response.getTime()))
                    .setWaitingTime(option.getWaiting_time() == null ? null : (int) Math.round(option.getWaiting_time()))
                    .setRoute(route));
        }
        return result;
    }
}
