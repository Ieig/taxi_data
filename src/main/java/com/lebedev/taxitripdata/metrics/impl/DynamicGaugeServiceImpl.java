package com.lebedev.taxitripdata.metrics.impl;

import com.lebedev.taxitripdata.metrics.DynamicGaugeService;
import com.lebedev.taxitripdata.model.entity.Route;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@RequiredArgsConstructor
public class DynamicGaugeServiceImpl implements DynamicGaugeService {
    private final MeterRegistry meterRegistry;
    private final List<String> taxiClassNames;
    private final Map<String, AtomicInteger> gauges = new ConcurrentHashMap<>();

    @Override
    public Set<Integer> addGauges(Set<Route> newGaugedRoutes, List<String> metricHandlerNames) {
        Set<Integer> processed = new HashSet<>();
        for (Route route : newGaugedRoutes) {
            String routeName = route.getDepartureName() + "--" + route.getDestinationName();
            for (String handler : metricHandlerNames) {
                for (String taxiClass : taxiClassNames) {
                    gauges.computeIfAbsent(route.getId() + "." + routeName + "." + handler + "." + taxiClass, v -> {
                        AtomicInteger gauge = new AtomicInteger(0);
                        Gauge.builder(route.getId() + "." + routeName + "." + handler + "." + taxiClass, gauge, AtomicInteger::get)
                                .description(handler + " for " + taxiClass + " on " + routeName)
                                .tags(route.getId().toString(), routeName, handler, taxiClass)
                                .register(meterRegistry);
                        return gauge;});
                }
            }
            processed.add(route.getId());
        }
        return processed;
    }

    @Override
    public Set<Integer> removeGauges(Set<Integer> gaugesToRemove) {
        gauges.keySet().removeIf(key -> gaugesToRemove.stream().anyMatch(id -> key.startsWith(id.toString())));
        return gaugesToRemove;
    }

    @Override
    public void setGauge(String gaugeName, Integer value) {
        AtomicInteger meter = Optional.ofNullable(gauges.get(gaugeName))
                .orElseThrow(() -> new RuntimeException("Metric is gone!"));
        meter.set(value);
    }

    // Metrics names preparation: prefix.suffix.postfix
    // prefix -> route id and name depend on routes from db (done periodically)
    // suffix -> metric name depend on available bean handlers (done once at start)
    // postfix -> taxi class depend on configuration in business.yml (done once at start)
}
