package com.lebedev.taxitripdata.metrics.impl;

import com.lebedev.taxitripdata.metrics.DynamicGaugeService;
import com.lebedev.taxitripdata.metrics.MetricsHandler;
import com.lebedev.taxitripdata.model.entity.RouteData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class WaitingTimeMetricsHandler implements MetricsHandler {
    private final DynamicGaugeService dynamicGaugeService;

    @Override
    public void handle(List<RouteData> routeData) {
        for (RouteData data : routeData) {
            String gaugeName = data.getRoute().getId() + "." +
                    data.getRoute().getDepartureName() + "--" +
                    data.getRoute().getDestinationName() + "." +
                    getMetric() + "." +
                    data.getClassName();
            if (data.getWaitingTime() != null) {
                dynamicGaugeService.setGauge(gaugeName, data.getWaitingTime());
            }
        }
    }

    @Override
    public String getMetric() {
        return "WAITING_TIME";
    }
}
