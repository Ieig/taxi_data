package com.lebedev.taxitripdata.metrics.impl;

import com.lebedev.taxitripdata.metrics.DynamicGaugeService;
import com.lebedev.taxitripdata.metrics.ScrapingService;
import com.lebedev.taxitripdata.metrics.MetricsHandler;
import com.lebedev.taxitripdata.model.entity.Route;
import com.lebedev.taxitripdata.model.entity.RouteData;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Service
@RequiredArgsConstructor
public class ScrapingServiceImpl implements ScrapingService {
    private final DynamicGaugeService dynamicGaugeService;
    private final List<MetricsHandler> handlers;
    private List<String> metricHandlerNames;
    private final Set<Integer> activeRouteIds = new HashSet<>();

    @PostConstruct
    void metricHandlerNames() {
        metricHandlerNames = handlers.stream().map(MetricsHandler::getMetric).toList();
    }


    @Override
    public void validateMetrics(List<Route> routes) {
        Set<Integer> newRouteIds = routes.stream().map(Route::getId).collect(toSet());
        if (activeRouteIds.equals(newRouteIds)) {
            return;
        }

        Set<Route> newRoutes = routes.stream()
                .filter(route -> !activeRouteIds.contains(route.getId()))
                .collect(toSet());

        if (!newRoutes.isEmpty()) {
            activeRouteIds.addAll(dynamicGaugeService.addGauges(newRoutes, metricHandlerNames));
        }

        Set<Integer> toRemove = activeRouteIds.stream()
                .filter(id -> !newRouteIds.contains(id))
                .collect(toSet());

        if (!toRemove.isEmpty()) {
            activeRouteIds.removeAll(dynamicGaugeService.removeGauges(toRemove));
        }
    }

    @Override
    @Async
    public void scrapeData(List<RouteData> routeData) {
        for (MetricsHandler handler : handlers) {
            handler.handle(routeData);
        }
    }
}
