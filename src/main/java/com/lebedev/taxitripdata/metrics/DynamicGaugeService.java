package com.lebedev.taxitripdata.metrics;

import com.lebedev.taxitripdata.model.entity.Route;

import java.util.List;
import java.util.Set;

public interface DynamicGaugeService {
    Set<Integer> addGauges(Set<Route> newGaugedRoutes, List<String> metricHandlerNames);
    Set<Integer> removeGauges(Set<Integer> gaugesToRemove);
    void setGauge(String name, Integer value);
}
