package com.lebedev.taxitripdata.metrics;

import com.lebedev.taxitripdata.model.entity.Route;
import com.lebedev.taxitripdata.model.entity.RouteData;

import java.util.List;

public interface ScrapingService {
    void validateMetrics(List<Route> routes);
    void scrapeData(List<RouteData> routeData);
}
