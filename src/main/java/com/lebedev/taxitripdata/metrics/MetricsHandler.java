package com.lebedev.taxitripdata.metrics;

import com.lebedev.taxitripdata.model.entity.RouteData;

import java.util.List;

public interface MetricsHandler {
    void handle(List<RouteData> routeData);
    String getMetric();
}
