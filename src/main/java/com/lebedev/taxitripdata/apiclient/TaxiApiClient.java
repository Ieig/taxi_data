package com.lebedev.taxitripdata.apiclient;

import com.lebedev.taxitripdata.model.reponse.TaxiResponse;
import io.micrometer.core.annotation.Timed;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "taxiClient", url = "${taxi-api.url}")
public interface TaxiApiClient {
    @Timed("getRouteDataFromYa")
    @GetMapping
    TaxiResponse getRouteData(@RequestParam String rll);
}
