package com.lebedev.taxitripdata.service;

import com.lebedev.taxitripdata.model.entity.Route;

import java.util.List;

public interface RouteService {
    List<Route> getRoutes();
}
