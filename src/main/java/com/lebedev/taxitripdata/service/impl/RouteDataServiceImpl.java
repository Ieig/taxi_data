package com.lebedev.taxitripdata.service.impl;

import com.lebedev.taxitripdata.apiclient.TaxiApiClient;
import com.lebedev.taxitripdata.mapper.TaxiResponseMapper;
import com.lebedev.taxitripdata.model.entity.Route;
import com.lebedev.taxitripdata.model.entity.RouteData;
import com.lebedev.taxitripdata.model.reponse.TaxiResponse;
import com.lebedev.taxitripdata.repository.RouteDataRepository;
import com.lebedev.taxitripdata.service.RouteDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RouteDataServiceImpl implements RouteDataService {
    private final RouteDataRepository routeDataRepository;
    private final TaxiApiClient taxiApiClient;
    private final TaxiResponseMapper mapper;

    @Override
    public List<RouteData> getRouteData(Route route) {
        TaxiResponse response = taxiApiClient.getRouteData(getRouteCoordinates(route));
        if (response == null) {
            throw new RuntimeException("No data fetched!");
        }
        return mapper.toRouteDataList(response, route);
    }

    @Override
    @Async
    public void saveDataAsync(List<RouteData> routeData) {
        routeDataRepository.saveAll(routeData);
    }

    private String getRouteCoordinates(Route route) {
        return route.getDepartureLongitude() + "," +
                route.getDepartureLatitude() + "~" +
                route.getDestinationLongitude() + "," +
                route.getDestinationLatitude();
    }
}
