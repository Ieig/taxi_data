package com.lebedev.taxitripdata.service.impl;

import com.lebedev.taxitripdata.model.entity.Route;
import com.lebedev.taxitripdata.repository.RouteRepository;
import com.lebedev.taxitripdata.service.RouteService;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RouteServiceImpl implements RouteService {
    private final RouteRepository routeRepository;

    @Timed("getRoutesFromDb")
    @Override
    public List<Route> getRoutes() {
        return routeRepository.findAll();
    }
}
