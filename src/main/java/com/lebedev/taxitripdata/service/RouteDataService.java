package com.lebedev.taxitripdata.service;

import com.lebedev.taxitripdata.model.entity.Route;
import com.lebedev.taxitripdata.model.entity.RouteData;

import java.util.List;

public interface RouteDataService {
    List<RouteData> getRouteData(Route route);

    void saveDataAsync(List<RouteData> routeData);
}
