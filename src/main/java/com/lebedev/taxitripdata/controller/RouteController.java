package com.lebedev.taxitripdata.controller;

import com.lebedev.taxitripdata.model.entity.Route;
import com.lebedev.taxitripdata.model.reponse.RouteResponse;
import com.lebedev.taxitripdata.service.RouteService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/routes")
@RequiredArgsConstructor
public class RouteController {
    private final RouteService routeService;
    private final Converter<Route, RouteResponse> converter;

    @GetMapping("/list")
    ResponseEntity<List<RouteResponse>> getRoutes() {
        return ok(routeService.getRoutes().stream()
                .map(converter::convert)
                .collect(Collectors.toList()));
    }

    // TO DO
    // addRoute
    // editRoute
    // deleteRoute
}
