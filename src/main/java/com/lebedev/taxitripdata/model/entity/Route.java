package com.lebedev.taxitripdata.model.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Accessors(chain = true)
@Table(name = "route_info")
public class Route {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "departure_lon", nullable = false)
    private String departureLongitude;

    @Column(name = "departure_lat", nullable = false)
    private String departureLatitude;

    @Column(name = "departure_name", nullable = false)
    private String departureName;

    @Column(name = "destination_lon", nullable = false)
    private String destinationLongitude;

    @Column(name = "destination_lat", nullable = false)
    private String destinationLatitude;

    @Column(name = "destination_name", nullable = false)
    private String destinationName;

    @ToString.Exclude
    @OneToMany(mappedBy = "route", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RouteData> entries = new ArrayList<>();
}
