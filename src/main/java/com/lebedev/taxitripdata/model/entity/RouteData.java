package com.lebedev.taxitripdata.model.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Entity
@Data
@Accessors(chain = true)
@Table(name = "route_data")
public class RouteData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "created", updatable = false, nullable = false)
    private LocalDateTime created;

    @Column(name = "class_name", nullable = false)
    private String className;

    @Column(name = "price", nullable = false)
    private Integer price;

    @Column(name = "distance", nullable = false)
    private Integer distance;

    @Column(name = "trip_time", nullable = false)
    private Integer tripTime;

    @Column(name = "waiting_time")
    private Integer waitingTime;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "route_id", nullable = false)
    private Route route;
}
