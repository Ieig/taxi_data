package com.lebedev.taxitripdata.model.reponse;

import lombok.Data;

import java.util.List;

@Data
public class TaxiResponse {
    private List<Option> options;
    private String currency;
    private Double distance;
    private Double time;
    private String time_text;
}
