package com.lebedev.taxitripdata.model.reponse;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RouteResponse {
    private Integer id;
    private String departureLongitude;
    private String departureLatitude;
    private String departureName;
    private String destinationLongitude;
    private String destinationLatitude;
    private String destinationName;
}
