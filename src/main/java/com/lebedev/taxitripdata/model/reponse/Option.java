package com.lebedev.taxitripdata.model.reponse;

import lombok.Data;

@Data
public class Option {
    private Double price;
    private Double min_price;
    private Double waiting_time;
    private String class_name;
    private String class_text;
    private Integer class_level;
    private String price_text;
}
