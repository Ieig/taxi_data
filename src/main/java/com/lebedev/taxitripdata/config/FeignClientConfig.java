package com.lebedev.taxitripdata.config;

import com.lebedev.taxitripdata.properties.TaxiProperties;
import feign.RequestInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class FeignClientConfig {
    private final TaxiProperties taxiProperties;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header(taxiProperties.getHeaderName(), taxiProperties.getApiKey());
            requestTemplate.query("clid", taxiProperties.getClid());
            requestTemplate.query("class", taxiProperties.getVehicleClass());
        };
    }
}
