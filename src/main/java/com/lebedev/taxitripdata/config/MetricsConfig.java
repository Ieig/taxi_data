package com.lebedev.taxitripdata.config;

import com.lebedev.taxitripdata.properties.TaxiProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
@RequiredArgsConstructor
public class MetricsConfig {
    private final TaxiProperties taxiProperties;

    @Bean
    List<String> taxiClassNames() {
        return Arrays.stream(taxiProperties.getVehicleClass().split(",")).toList();
    }

}
