package com.lebedev.taxitripdata.scheduler;

import com.lebedev.taxitripdata.metrics.ScrapingService;
import com.lebedev.taxitripdata.model.entity.Route;
import com.lebedev.taxitripdata.model.entity.RouteData;
import com.lebedev.taxitripdata.service.RouteDataService;
import com.lebedev.taxitripdata.service.RouteService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class TaxiScheduler {
    private final RouteService routeService;
    private final RouteDataService routeDataService;
    private final ScrapingService scrapingService;
    private final TaskScheduler taskScheduler;
    private final long rate;

    public TaxiScheduler(RouteService routeService,
                         RouteDataService routeDataService,
                         ScrapingService scrapingService,
                         @Qualifier("dbOpsTaskScheduler") TaskScheduler taskScheduler,
                         @Value("${schedule.api-calls.rate}") long rate) {
        this.routeService = routeService;
        this.routeDataService = routeDataService;
        this.scrapingService = scrapingService;
        this.taskScheduler = taskScheduler;
        this.rate = rate;
    }


    @Timed("mainTaxiScheduler")
    @Scheduled(fixedRateString = "${schedule.main.rate}")
    public void updateTaxiData() {
        List<Route> routes = routeService.getRoutes();
        AtomicInteger counter = new AtomicInteger();
        scrapingService.validateMetrics(routes);

        for (Route route : routes) {
            taskScheduler.schedule(() -> {
                List<RouteData> routeData = routeDataService.getRouteData(route);
                scrapingService.scrapeData(routeData);
                routeDataService.saveDataAsync(routeData);
            }, Instant.ofEpochMilli(System.currentTimeMillis() + rate * counter.getAndIncrement()));
        }
    }
}
