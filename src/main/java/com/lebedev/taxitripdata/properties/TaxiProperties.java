package com.lebedev.taxitripdata.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "taxi-api")
public class TaxiProperties {
    private String headerName;
    private String clid;
    private String apiKey;
    private String vehicleClass;
}
